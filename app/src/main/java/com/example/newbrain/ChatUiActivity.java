package com.example.newbrain;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public  class ChatUiActivity extends AppCompatActivity implements SocketUtils.OnSocketResult {

    private final int VIEW_TYPE = 0xb01;
    private final int VIEW_TYPE_LEFT = -10;
    private final int VIEW_TYPE_RIGHT = -11;
    private final int MESSAGE = 0xb02;
    private final String server_ip="127.0.0.1";//192.168.1.100
    private final int server_port=21566;
    private ArrayList<HashMap<Integer, Object>> items =  new ArrayList<HashMap<Integer, Object>>();
    private MyAdapter myAdapter;
    private SocketUtils conn;
    private  ListView lstView;
    private  Button btn_send;
    private  EditText editText_send;
    private  ImageButton imgBtn_more;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_ui);
        //Window window = getWindow();
       //window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        lstView = (ListView) findViewById(android.R.id.list);
        editText_send = (EditText) findViewById(R.id.edit_send);
        btn_send = (Button) findViewById(R.id.btn_send);
        imgBtn_more = (ImageButton) findViewById(R.id.btn_more);

        myAdapter = new MyAdapter(this, -1);
        lstView.setAdapter(myAdapter);
        conn=SocketUtils.getInstance();

//        final EditText editText_send = (EditText) findViewById(R.id.edit_send);
//        final Button btn_send = (Button) findViewById(R.id.btn_send);

        btn_send.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String msg = editText_send.getText() + "";
                if(msg=="") return;
                HashMap<Integer, Object> map = new HashMap<Integer, Object>();
                map.put(VIEW_TYPE, VIEW_TYPE_RIGHT);
                map.put(MESSAGE, msg);
                items.add(map);
                myAdapter.notifyDataSetChanged();
                // 发送后清空输入框内容
                editText_send.setText(null);
                // 将ListView滚动到最底部
                lstView.setSelection(ListView.FOCUS_DOWN);
                conn.sendMsg(server_ip,server_port,msg,ChatUiActivity.this);
            }
        });

        //如果软键盘收起，将ListView滚动到最底部
        KeyboardChangeListener softKeyboardStateHelper = new KeyboardChangeListener(this);
        softKeyboardStateHelper.setKeyBoardListener(new KeyboardChangeListener.KeyBoardListener() {
            @Override
            public void onKeyboardChange(boolean isShow, int keyboardHeight) {
                if (isShow) {
                    //键盘的弹出
                    editText_send.setCursorVisible(true);
                    lstView.setSelection(ListView.FOCUS_DOWN);
                } else {
                    //键盘的收起
                    editText_send.setCursorVisible(false);
                }
            }
        });

    }


//    String last_tm_period="";
//    protected void onResume(){
//        super.onResume();
//        long l = System.currentTimeMillis();
//        Date date = new Date(l);
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);
//        int cur_hour=cal.get(Calendar.HOUR_OF_DAY);
//        Log.w("tcp",cur_hour+"");
//        String cur_tm_period="";
//        if (cur_hour>=23 || cur_hour<5)
//            cur_tm_period="night";
//        else if (cur_hour>=5 && cur_hour<7)
//            cur_tm_period="earlymorning";
//        else if (cur_hour>=7 && cur_hour<12)
//            cur_tm_period="morning";
//        else if (cur_hour>=12 && cur_hour<13)
//            cur_tm_period="noon";
//        else if (cur_hour>=13 && cur_hour<17)
//            cur_tm_period="afternoon";
//        else if (cur_hour>=17 && cur_hour<19)
//            cur_tm_period="dusk";
//        else if (cur_hour>=19 && cur_hour<23)
//            cur_tm_period="evening";
//        if(!cur_tm_period.equals(last_tm_period)){
//            last_tm_period=cur_tm_period;
//            conn.sendMsg(server_ip,server_port,cur_tm_period,ChatUiActivity.this);
//        }
//    }

    @Override
    public void onSuccess(String result) {
        Log.w("tcp","__________enter onSuccess");
        HashMap<Integer, Object> map = new HashMap<Integer, Object>();
        map.put(VIEW_TYPE, VIEW_TYPE_LEFT);
        map.put(MESSAGE, result);
        items.add(map);
        myAdapter.notifyDataSetChanged();
        // 将ListView滚动到最底部
        lstView.setSelection(ListView.FOCUS_DOWN);
    }

private class MyAdapter extends ArrayAdapter {

    private LayoutInflater layoutInflater;

    public MyAdapter(Context context, int resource) {
        super(context, resource);
        layoutInflater = LayoutInflater.from(context);
    }
    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        int type = getItemViewType(pos);
        String msg = getItem(pos);
        switch (type) {
            case VIEW_TYPE_LEFT:
                convertView = layoutInflater.inflate(R.layout.base_left_usr, null);
                convertView.setAlpha(0.7f);
                TextView textLeft = (TextView) convertView.findViewById(R.id.usr_msg);
                textLeft.setText(msg+"");
                break;
            case VIEW_TYPE_RIGHT:
                convertView = layoutInflater.inflate(R.layout.base_right_usr, null);
                convertView.setAlpha(0.7f);
                TextView textRight = (TextView) convertView.findViewById(R.id.usr_msg);
                textRight.setText(msg+"");
                break;
        }
        return convertView;
    }
    @Override
    public String getItem(int pos) {
        String s = items.get(pos).get(MESSAGE) + "";
        return s;
    }
    @Override
    public int getCount() {
        return items.size();
    }
    @Override
    public int getItemViewType(int pos) {
        int type = (Integer) items.get(pos).get(VIEW_TYPE);
        return type;
    }
    @Override
    public int getViewTypeCount() {
        return 2;
    }
}
}